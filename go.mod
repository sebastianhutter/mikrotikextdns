module gitlab.com/sebastianhutter/mikrotikextdns

go 1.16

require (
	github.com/blang/semver v3.5.1+incompatible // indirect
	github.com/docker/distribution v2.7.1+incompatible // indirect
	github.com/ghodss/yaml v1.0.0 // indirect
	github.com/go-openapi/spec v0.20.3 // indirect
	github.com/gogo/protobuf v1.3.2 // indirect
	github.com/golang/glog v0.0.0-20210429001901-424d2337a529 // indirect
	github.com/google/gofuzz v1.2.0 // indirect
	github.com/howeyc/gopass v0.0.0-20190910152052-7cb4b85ec19c // indirect
	github.com/imdario/mergo v0.3.12 // indirect
	github.com/joho/godotenv v1.3.0
	github.com/juju/ratelimit v1.0.1 // indirect
	github.com/opencontainers/go-digest v1.0.0 // indirect
	github.com/pborman/uuid v1.2.1 // indirect
	github.com/prometheus/client_golang v1.10.0
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/ugorji/go v1.2.6 // indirect
	golang.org/x/net v0.0.0-20210510120150-4163338589ed // indirect
	gopkg.in/inf.v0 v0.9.1 // indirect
	gopkg.in/routeros.v2 v2.0.0-20190905230420-1bbf141cdd91
	k8s.io/api v0.21.0
	k8s.io/apimachinery v0.21.0
	k8s.io/client-go v0.21.0
)
