package main

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	log "github.com/sirupsen/logrus"
	"gopkg.in/routeros.v2"
	v1 "k8s.io/api/networking/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	"net/http"
	"os"
)

var (
	// mikrotik configuration
	mikrotik *routeros.Client
	address  = flag.String("address", "192.168.0.1:8728", "Address (env: MIKROTIK_ADDRESS)")
	username = flag.String("username", "admin", "Username (env: MIKROTIK_USERNAME)")
	password = flag.String("password", "admin", "Password (env: MIKROTIK_PASSWORD)")
	comment  = "tooling.hutter.cloud/mikrotik-static-ip"

	// k8s config
	clientset  *kubernetes.Clientset
	namespaces = flag.String("namespace", "", "Check namespace for ingresses (env: NAMESPACES)")
	annotation = "tooling.hutter.cloud/mikrotik-static-ip"

	// programm configuration
	loglevel = flag.String("loglevel", "info", "Loglevel (env: LOGLEVEL)")

	// http servers
	prom_http = http.NewServeMux()

	// prometheus metrics
	upserts = promauto.NewCounter(prometheus.CounterOpts{
		Name: "mikrotikextdns_upserts_total",
		Help: "The total number of executed dns upserts",
	})
	deletes = promauto.NewCounter(prometheus.CounterOpts{
		Name: "mikrotikextdns_deletes_total",
		Help: "The total number of executed dns deletes",
	})
)

func init() {
	flag.Parse()

	// overwrite configuration options with env vars
	if len(os.Getenv("MIKROTIK_ADDRESS")) > 0 {
		*address = os.Getenv("MIKROTIK_ADDRESS")
	}
	if len(os.Getenv("MIKROTIK_USERNAME")) > 0 {
		*username = os.Getenv("MIKROTIK_USERNAME")
	}
	if len(os.Getenv("MIKROTIK_PASSWORD")) > 0 {
		*password = os.Getenv("MIKROTIK_PASSWORD")
	}
	if len(os.Getenv("NAMESPACES")) > 0 {
		*namespaces = os.Getenv("NAMESPACES")
	}
	if len(os.Getenv("LOGLEVEL")) > 0 {
		*loglevel = os.Getenv("LOGLEVEL")
	}

	// setup logging
	var err error
	l, err := log.ParseLevel(*loglevel)
	if err != nil {
		log.Fatalln("Invalid loglevel specified")
	}
	log.SetLevel(l)

	// setup kubernetes connection
	log.Debugln("Setup kubernetes connection")
	var c *rest.Config
	if len(os.Getenv("KUBECONFIG")) > 0 {
		c, err = clientcmd.BuildConfigFromFlags("", os.Getenv("KUBECONFIG"))
	} else {
		log.Warnln("No kubeconfig env var set. Trying to load in cluster conf")
		c, err = rest.InClusterConfig()
	}
	if err != nil {
		log.Fatalln(err.Error())
	}

	clientset, err = kubernetes.NewForConfig(c)
	if err != nil {
		log.Fatalln(err.Error())
	}

	log.Debugln("Connect to mikrotik")
	mikrotik, err = routeros.Dial(*address, *username, *password)
	if err != nil {
		log.Fatal(err.Error())
	}

	log.Debugln("Initialize prometheus handler")
	prom_http.Handle("/metrics", promhttp.Handler())
}

// getDnsEntryValue returns from the static dns entry
func getDnsEntryValue(h string, k string) (string, error) {

	// get dns entry by name
	r, err := mikrotik.Run("/ip/dns/static/print", fmt.Sprintf("?name=%s", h))
	if err != nil {
		return "", err
	}

	if len(r.Re) > 1 {
		return "", errors.New(fmt.Sprintf("Multiple DNS entries found for %s. Please check entries in router!", h))
	}

	// return an empty string if no dns entry was found
	if len(r.Re) == 0 {
		return "", nil
	}

	// retrieve value of given keyu
	var ret string
	for _, v := range r.Re[0].List {
		if v.Key == k {
			ret = v.Value
		}
	}
	return ret, nil
}

// getDnsEntryId returns the static dns entry id from mikrotik
func getDnsEntryId(h string) (string, error) {
	return getDnsEntryValue(h, ".id")
}

// getDnsEntryComment returns the static dns entry comment from mikrotik
func getDnsEntryComment(h string) (string, error) {
	return getDnsEntryValue(h, "comment")
}

// getDnsEntryAdress returns the static dns entry address from mikrotik
func getDnsEntryAddress(h string) (string, error) {
	return getDnsEntryValue(h, "address")
}

// upsertDnsEntry creates or updates the static dns entry for the router
func upsertDnsEntry(h string, ip string) error {
	log.Infof("Upsert DNS entry for host %s\n", h)

	id, err := getDnsEntryId(h)
	if err != nil {
		return err
	}

	if len(id) > 0 {
		log.Debugln("updating dns entry")
		c, err := getDnsEntryComment(h)
		if err != nil {
			return err
		}
		a, err := getDnsEntryAddress(h)
		if err != nil {
			return err
		}

		if c == comment {
			if a != ip {
				_, err := mikrotik.Run("/ip/dns/static/set", fmt.Sprintf("=.id=%s", id), fmt.Sprintf("=address=%s", ip), fmt.Sprintf("=comment=%s", comment))
				if err != nil {
					return err
				}
				upserts.Inc()
			}
		} else {
			log.Warnln("dns entry exist but comment does not match. please check dns entries manually")
		}
	} else {
		log.Debugln("creating dns entry")
		_, err := mikrotik.Run("/ip/dns/static/add", fmt.Sprintf("=name=%s", h), fmt.Sprintf("=address=%s", ip), fmt.Sprintf("=comment=%s", comment))
		if err != nil {
			return err
		}
		upserts.Inc()
	}

	return nil
}

// deleteDnsEntry removes the static dns entry from the mikrotik router
func deleteDnsEntry(h string) error {
	log.Infof("Delete DNS entry for host %s\n", h)

	id, err := getDnsEntryId(h)
	if err != nil {
		return err
	}

	if len(id) > 0 {
		c, err := getDnsEntryComment(h)
		if err != nil {
			return err
		}
		if c == comment {
			_, err := mikrotik.Run("/ip/dns/static/remove", fmt.Sprintf("=.id=%s", id))
			if err != nil {
				return err
			}
			deletes.Inc()
		} else {
			log.Warnln("dns entry exist but comment does not match. please check dns entries manually")
		}
	}
	return nil
}

func main() {

	log.Debugln("Setup watcher")
	watcher, err := clientset.NetworkingV1().Ingresses(*namespaces).Watch(context.TODO(), metav1.ListOptions{})
	//
	if err != nil {
		log.Fatalln(err.Error())
	}
	ch := watcher.ResultChan()

	log.Debugln("Start prometheus http service")
	go http.ListenAndServe(":2112", prom_http)

	log.Debugln("Entering watch loop")
	for {

		event := <-ch
		// action is either ADDED, MODIFIED or DELETED
		action := event.Type
		ingress, ok := event.Object.(*v1.Ingress)
		if !ok {
			log.Fatalln("Could not cast to Ingress")
		}
		ip, _ := ingress.Annotations[annotation]

		for _, v := range ingress.Spec.Rules {
			// if an ip is found we need to upsert or remove the entry
			// if no ip is found the ingress resource shouldnt be added to mikrotik
			// and we should make sure to remove any potential dns entry in mikrotik
			// which were created manually
			if len(ip) > 0 {
				log.Debugf("Action: %s, Host: %s, Ip: %s", action, v.Host, ip)
				switch action {
				case "ADDED":
					err := upsertDnsEntry(v.Host, ip)
					if err != nil {
						log.Errorln(err.Error())
					}
				case "MODIFIED":
					err := upsertDnsEntry(v.Host, ip)
					if err != nil {
						log.Errorln(err.Error())
					}
				case "DELETED":
					err := deleteDnsEntry(v.Host)
					if err != nil {
						log.Errorln(err.Error())
					}
				default:
					// do nothin
				}
			} else {
				log.Debugf("Action: %s, Host: %s BUT NO ANNOTATION IP FOUND!", action, v.Host)
				err := deleteDnsEntry(v.Host)
				if err != nil {
					log.Errorln(err.Error())
				}
			}
		}
	}
}
