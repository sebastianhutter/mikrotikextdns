# Changelog

## 0.2.0
* create mikrotik dns entries with comment to ensure service does not remove or overwrite *unmanaged* entries
* add prometheus instrumentation
  * add upserts and delete counters
* add prometheus service monitor (.Values.prometheus.enabled)

## 0.1.0
* initial working release
* helm chart for deployment
