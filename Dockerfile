FROM scratch

ADD mikrotikextdns_linux_amd64 /mikrotikextdns
ENTRYPOINT [ "/mikrotikextdns" ]
